FROM python:3.8-slim

WORKDIR /app

COPY . .

RUN pip install --no-cache-dir -r requirements.txt

ENV FLASK_APP=src/app.py

CMD ["python", "setup.py"] && ["flask", "run", "--host=0.0.0.0", "--port=8000"]

