## Getting started

     docker-compose up --build app


     curl localhost:8000/api/v1/users \
          -X POST -d '{"firstName":"Docker","lastName":"testUser"}' \
          --header "Content-Type: application/json"

     curl localhost:8000/api/v1/users/1
